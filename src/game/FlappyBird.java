package game;

import java.util.LinkedList;
import java.util.List;

import game.factory.BirdFactory;
import game.factory.PipeFactory;
import game.model.Passaro;
import game.model.Cano;
import neuroevolution.genetic.AlgoritmoGenetico;
import neuroevolution.genetic.Genotipo;
import neuroevolution.neuralnetwork.Camada;
import neuroevolution.neuralnetwork.RedeNeural;
import neuroevolution.neuralnetwork.Neuronio;
import processing.core.PApplet;
import processing.core.PImage;
import util.Screen;

public class FlappyBird extends PApplet {
	
	List<Cano> pipes;
	AlgoritmoGenetico agent;
	
	float pipeSwapnX;
	float pipeStart;
	int score = 0;
	int highscore = 0;
	int tickCounter = 0;
	int speed = 1;
	int maxSpeed = 10;
	
	PImage passaroIco;
	
	int pipeSpawnRate;
	
	PImage backgroundImage;
	
	public void settings() {
		size(1366, 768);
		Screen.setDimensions(width, height);
		BirdFactory.init(Screen.WIDTH/5.0f, Screen.HEIGHT/2.0f, 20);
		this.pipeSwapnX = width;
		this.pipeStart = width / 3f;
		this.agent = new AlgoritmoGenetico();
		this.pipes = new LinkedList<Cano>();
		this.initCanos();
		this.pipeSpawnRate = Math.abs(Math.round(pipeStart / this.pipes.get(0).velocity));
	}
	
	public void setup() {
		backgroundImage = loadImage("resources/background.png");
		surface.setTitle("Flappy Bird Automatico");
	}
	
	public void draw() {
		for (int i = 0; i < speed; i++) {
			tickCounter++;
			clearScreen();
			pipes.forEach(pipe -> renderCano(pipe));
			agent.population.genomes.forEach(genome -> renderBird(genome.bird));
			drawGenerationInfo();
			renderRedeNeural();
			pipes.removeIf(pipe -> pipe.isInvisible());
			for (Genotipo genome: this.agent.population.genomes) {
				for (Cano pipe: this.pipes) {
					checkCollision(pipe, genome.bird);
				}
			}
			if (tickCounter % pipeSpawnRate == 0) {
				spawnCano(this.pipeSwapnX);
			}
			pipes.forEach(pipe -> pipe.update());
			agent.updatePopulacao(pipes);
			if (agent.populationDead()) {
				reset();
			}
		}
	}
	
	public void keyPressed() {
		if (key == CODED) {
			if (keyCode == UP) {
				speed = min(maxSpeed, ++speed);
			} else if (keyCode == DOWN) {
				speed = max(1, --speed);
			}
		}
	}
	
	private void checkCollision(Cano pipe, Passaro bird) {
		if (bird.isDead)
			return;
		if (pipe.checkPassaroCollision(bird)) {
			bird.isDead = true;
			agent.alive--;
		} else {
			if (pipe.checkPass(bird)) {
				bird.gameScore++;
			}
		}
	}
	
	private void reset() {
		pipes.clear();
		initCanos();
		frameCount = 0;
		tickCounter = 0;
		score = 0;
		agent.evolvePopulacao();
	}
	
	private void initCanos() {
		spawnCano(pipeStart);
		spawnCano(2*pipeStart);
		spawnCano(3*pipeStart);
	}
	
	private void spawnCano(float x) {
		pipes.add(PipeFactory.getPype(x));
	}
	
	private void clearScreen() {
		image(backgroundImage, 0, 0);
	}
	
	private void drawGenerationInfo() {
		textSize(32);
		fill(255);
		score = agent.getBestScore();
		highscore = score > highscore ? score : highscore;
		text("Pontuação: " + score, 20, 50);
		text("Geração: " + agent.generation, 20, 100);
		text("Vivos: " + agent.alive + " / " + agent.populationSize, 20, 150);
		text("Highscore: " + highscore, 20, 200);
		text("Velocidade: " + speed + "x", 20, 250);
	}
	
	private void renderRedeNeural() {
		RedeNeural net = this.agent.getBestGenome();
		int beginx = 1100;
		int beginy = 20;
		int yspan = 300;
		int layerSpace = 80;
		int neuronSpace = 10;
		int layerWidth = 25;
		ellipseMode(CENTER);
		for (int i = 1; i < net.Camadas.size(); i++) {
			Camada prevCamada = net.Camadas.get(i-1);
			Camada layer = net.Camadas.get(i);
			int totalCamadaHeight = layer.Neuronios.size()*layerWidth + (layer.Neuronios.size()-1)*neuronSpace;
			int layerBegin = beginy + (yspan - totalCamadaHeight)/2;
			int totalPrevCamadaHeight = prevCamada.Neuronios.size()*layerWidth + (prevCamada.Neuronios.size()-1)*neuronSpace;
			int prevCamadaBegin = beginy + (yspan - totalPrevCamadaHeight)/2;
			// Draw current layer
			for (int j = 0; j < layer.Neuronios.size(); j++) {
				// Draw weights for each neuron
				Neuronio neuron = layer.Neuronios.get(j);
				for (int k = 0; k < neuron.weights.size(); k++) {
					float weight = neuron.weights.get(k);
					strokeWeight(2*Math.abs(weight));
					if (weight >= 0) {
						stroke(0, 0, 255);
					} else {
						stroke(255, 0, 0);
					}
					line(beginx + (i-1)*(layerWidth+layerSpace), prevCamadaBegin + k*(layerWidth + neuronSpace), beginx + i*(layerWidth+layerSpace), layerBegin + j*(neuronSpace + layerWidth));
				}
				fill(255);
				strokeWeight(1);
				stroke(0);
				ellipse(beginx + i*(layerWidth+layerSpace), layerBegin + j*(neuronSpace + layerWidth), layerWidth, layerWidth);
			}
			// Draw previous layer
			for (int j = 0; j < prevCamada.Neuronios.size(); j++) {
				fill(255);
				strokeWeight(1);
				stroke(0);
				ellipse(beginx + (i-1)*(layerWidth+layerSpace), prevCamadaBegin + j*(neuronSpace + layerWidth), layerWidth, layerWidth);
			}
		}
	}

	private void renderBird(Passaro bird) {
		if (!bird.isDead) {
			AlgoritmoGenetico.CanoInfo pipeInfo = this.agent.getClosestCanoInfo();
			if (pipeInfo != null && pipeInfo.closestCano != null) {
				Cano pipe = pipeInfo.closestCano;
				stroke(255, 0, 0);
				strokeWeight(2);
				line(bird.x, bird.y, pipe.x + pipe.width/2, height - pipe.height - pipe.gap);
				line(bird.x, bird.y, pipe.x + pipe.width/2, height - pipe.height - pipe.gap/2);
				line(bird.x, bird.y, pipe.x + pipe.width/2, height - pipe.height);
			}
			stroke(0);
			strokeWeight(1);
			passaroIco = loadImage("resources/passaro.png");
			passaroIco.resize(32, 0);
			fill(224, 227, 20);
			ellipseMode(CENTER);
			image(passaroIco, bird.x, bird.y);
		}
	}
	
	private void renderCano(Cano pipe) {
		stroke(0);
		strokeWeight(1);
		fill(9, 148, 46);
		rect(pipe.x, 0, pipe.width, height - pipe.height - pipe.gap);
		rect(pipe.x, 768 - pipe.height, pipe.width, pipe.height);
	}
}