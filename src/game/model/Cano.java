package game.model;

import java.util.HashMap;

import util.Screen;

public class Cano {
	
	public float height;
	public float x;
	public float velocity = -4f;
	public float width = 60f;
	
	public final float gap = 180;
	
	private HashMap<Passaro, Boolean> birdHasPassed = new HashMap<Passaro, Boolean>();
	
	public Cano(float x, float height) {
		this.x = x;
		this.height = height;
	}
	
	public void update() {
		this.x += velocity;
	}
	
	public boolean isInvisible() {
		return this.x < -this.width;
	}
	
	public boolean checkPassaroCollision(Passaro bird) {
		if (checkBottomCano(bird))
			return true;
		return checkTopCano(bird);
	}
	
	public boolean checkPass(Passaro bird) {
		boolean birdPass = this.birdHasPassed.containsKey(bird);
		if (bird.x - bird.radius > this.x + this.width && !birdPass) {
			this.birdHasPassed.put(bird, true);
			return true;
		}
		return false;
	}
	
	private boolean checkTopCano(Passaro bird) {
		return checkPassaroRect(bird, this.x, 0, this.width, Screen.HEIGHT - this.gap - this.height);
	}
	
	private boolean checkBottomCano(Passaro bird) {
		return checkPassaroRect(bird, this.x, Screen.HEIGHT - this.height, this.width, this.height);
	}
	
	private boolean checkPassaroRect(Passaro bird, float x, float y, float w, float h) {
		float testx = bird.x;
		float testy = bird.y;
		if (bird.x < x)
			testx = x;
		else if (bird.x > x + w) 
			testx = x + w;
		if (bird.y < y)
			testy = y;
		else if (bird.y > y + h)
			testy = y + h;
		float distx = bird.x - testx;
		float disty = bird.y - testy;
		return (distx*distx + disty*disty) <= bird.radius*bird.radius;
	}
}