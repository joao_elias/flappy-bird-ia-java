package game.model;

import neuroevolution.neuralnetwork.RedeNeural;
import util.Screen;

public class Passaro {
	public float x;
	public float y;
	public float radius;
	public float velocity;
	public float gravity;
	public float airDrag;
	public float flapForce;
	public boolean isDead = false;
	public int score;
	public int gameScore;
	
	public RedeNeural net;
	
	private Passaro() {
		this.velocity = 0.4f;
		this.gravity = 0f;
		this.airDrag = 0.9f;
		this.flapForce = -6f;
		this.isDead = false;
		this.gameScore = 0;
		this.score = 0;
	}
	
	public Passaro(float x, float y, float r) {    
		this();
		this.x = x;
		this.y = y;
		this.radius = r;
		this.net = new RedeNeural(3, 8, 2);
	}
	
	public Passaro(float x, float y, float r, RedeNeural.FlattenNetwork net) {
		this();
		this.x = x;
		this.y = y;
		this.radius = r;
		this.net = RedeNeural.expand(net);
	}
	
	public void update() {
		this.score++;
		//this.velocity *= airDrag;
		gravity += velocity;
		this.y += gravity;
	}
	
	public void feed(Cano closestCano, float distance) {
		if (closestCano != null && !this.isDead) {
			float[] inputs = {
				distance / Screen.WIDTH,
				(this.y + this.radius - (Screen.HEIGHT - closestCano.height)) / Screen.HEIGHT,
				((Screen.HEIGHT - closestCano.gap - closestCano.height) - this.y - this.radius) / Screen.HEIGHT,
			};
			float[] output = this.net.eval(inputs);
			if (output[0] > output[1])
				this.flap();
		}
	}
	
	public void flap() {
		gravity = flapForce;
	}
}