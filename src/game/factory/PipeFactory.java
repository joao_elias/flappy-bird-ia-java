package game.factory;

import java.util.Random;

import game.model.Cano;

public class PipeFactory {
	
	private static Random random = new Random();
	private static float minHeight = 200f;
	private static float maxHeight = 500f;
	
	private PipeFactory() {
		
	}
	
	public static Cano getPype(float x) {
		return new Cano(x, minHeight + random.nextFloat() * (maxHeight - minHeight));
	}
	
}