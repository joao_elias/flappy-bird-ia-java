package game.factory;

import game.model.Passaro;
import neuroevolution.neuralnetwork.RedeNeural;

public class BirdFactory {
	
	private static float spawnX;
	private static float spawnY;
	private static float radius;

	private BirdFactory() {
		
	}
	
	public static void init(float x, float y, float r) {
		spawnX = x;
		spawnY = y;
		radius = r;
	}
	
	public static Passaro getPassaro() {
		return new Passaro(spawnX, spawnY, radius);
	}
	
	public static Passaro getPassaro(RedeNeural.FlattenNetwork net) {
		return new Passaro(spawnX, spawnY, radius, net);
	}
	
	public static float getSpawnX() {
		return spawnX;
	}
	
	public static float getRadius() {
		return spawnY;
	}}