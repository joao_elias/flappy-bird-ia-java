package neuroevolution.genetic;

import java.util.ArrayList;
import java.util.List;

import neuroevolution.neuralnetwork.RedeNeural;

public class Populacao {

	public List<Genotipo> genomes;
	
	public Populacao(int PopulacaoSize) {
		this.genomes = new ArrayList<Genotipo>();
		for (int i = 0; i < PopulacaoSize; i++) {
			this.genomes.add(new Genotipo());
		}
	}
	
	public void evolve(float elitism, float randomness, float mutationRate, float mutationStdDev, int childCount) {
		this.normalFitnessDistribution();
		this.sortByFitness();
		List<Genotipo> nextGeneration = new ArrayList<Genotipo>();
		int eliteCount = Math.round(elitism*this.genomes.size());
		for (int i = 0; i < eliteCount; i++) {
			nextGeneration.add(new Genotipo(this.genomes.get(i)));
		}
		int randomCount = Math.round(randomness*this.genomes.size());
		for (int i = 0; i < randomCount; i++) {
			RedeNeural.FlattenNetwork net  = this.genomes.get(0).bird.net.flatten();
			for (int j = 1; j < net.weights.size(); j++) {
				net.weights.set(j, (float) (Math.random()*2 - 1));
			}
			nextGeneration.add(new Genotipo(net));
		}
		// Pool selection
		int max = 0;
		while (true) {
			for (int i = 0; i < max; i++) {
				List<Genotipo> children = Genotipo.breed(this.genomes.get(i), this.genomes.get(max), childCount, mutationRate, mutationStdDev);
				for (Genotipo child: children) {
					nextGeneration.add(child);
					if (nextGeneration.size() >= this.genomes.size()) {
						this.genomes = nextGeneration;
						return;
					}
				}
			}
			max++;
			max = max >= this.genomes.size()-1 ? 0 : max;
		}
	}

	private void normalFitnessDistribution() {
		float sum = 0f;
		for (Genotipo genome: this.genomes) {
			sum += genome.bird.score;
		}
		for (Genotipo genome: this.genomes) {
			genome.fitness = genome.bird.score / sum;
		}
	}
	
	// TODO: Implement quick sort or something else 
	private void sortByFitness() {
		for (int i = 0; i < this.genomes.size()-1; i++) {
			int bestIndex = i;
			for (int j = i+1; j < this.genomes.size(); j++) {
				if (this.genomes.get(j).fitness > this.genomes.get(bestIndex).fitness) {
					bestIndex = j;
				}
			}
			if (bestIndex != i) {
				Genotipo temp = this.genomes.get(bestIndex);
				this.genomes.set(bestIndex, this.genomes.get(i));
				this.genomes.set(i, temp);
			}
		}
	}
}