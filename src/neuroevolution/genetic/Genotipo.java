package neuroevolution.genetic;

import java.util.ArrayList;
import java.util.List;

import game.factory.BirdFactory;
import game.model.Passaro;
import neuroevolution.neuralnetwork.RedeNeural;

public class Genotipo {

	public Passaro bird;
	public float fitness;
	
	public Genotipo() {
		this.bird = BirdFactory.getPassaro();
		this.fitness = 0;
	}
	
	public Genotipo(RedeNeural.FlattenNetwork net) {
		this.bird = BirdFactory.getPassaro(net);
		this.fitness = 0;
	}
	
	public Genotipo(Genotipo genome) {
		this.bird = BirdFactory.getPassaro(genome.bird.net.flatten());
		this.fitness = 0;
	}
	
	public static List<Genotipo> breed(Genotipo male, Genotipo female, int childCount, float mutationRate, float mutationStdDev) {
		List<Genotipo> children = new ArrayList<Genotipo>();
		for (int ch = 0; ch < childCount; ch++) {
			RedeNeural.FlattenNetwork childNet = male.bird.net.flatten();
			RedeNeural.FlattenNetwork parentNet = female.bird.net.flatten();
			for (int i = 0; i < childNet.weights.size(); i++) {
				if (Math.random() <= 0.5) {
					childNet.weights.set(i, parentNet.weights.get(i));
				}
			}
			for (int i = 0; i < childNet.weights.size(); i++) {
				if (Math.random() <= mutationRate) {
					childNet.weights.set(i, (float) Math.random()*2*mutationStdDev - mutationStdDev);
				}
			}
			children.add(new Genotipo(childNet));
		}
		return children;
	}
}