package neuroevolution.genetic;

import java.util.List;

import game.factory.BirdFactory;
import game.model.Cano;
import neuroevolution.neuralnetwork.RedeNeural;
import util.Screen;

public class AlgoritmoGenetico {
	
	public class CanoInfo {
		public float distance;
		public Cano closestCano;
		
		public CanoInfo(float distance, Cano closestCano) {
			this.distance = distance;
			this.closestCano = closestCano;
		}
	}

	public Populacao population;
	public int alive;
	public int generation;
	
	public int populationSize = 100;
	public float elitism = 0.2f;
	public float mutationRate = 0.1f;
	public float mutationStdDev = 0.5f;
	public float randomness = 0.2f;
	public int childCount = 1;
	
	private RedeNeural bestGenome;
	private CanoInfo closestCanoInfo;
	
	public AlgoritmoGenetico() {
		this.population = new Populacao(this.populationSize);
		this.bestGenome = this.population.genomes.get(0).bird.net;
		this.alive = this.populationSize;
		this.generation = 1;
	}
	
	public void updatePopulacao(List<Cano> pipes) {
		getClosestCano(pipes);
		for (Genotipo genome: this.population.genomes) {
			if (!genome.bird.isDead) {
				genome.bird.feed(this.closestCanoInfo.closestCano, this.closestCanoInfo.distance);
				genome.bird.update();
				if (genome.bird.y < genome.bird.radius || genome.bird.y > Screen.HEIGHT-genome.bird.radius) {
					genome.bird.isDead = true;
					this.alive--;
				}
			}
		}
	}
	
	public void evolvePopulacao() {
		this.alive = this.populationSize;
		this.generation++;
		this.population.evolve(this.elitism, this.randomness, this.mutationRate, this.mutationStdDev, this.childCount);
		this.bestGenome = this.population.genomes.get(0).bird.net;
	}
	
	public RedeNeural getBestGenome() {
		return this.bestGenome;
	}
	
	public CanoInfo getClosestCanoInfo() {
		return this.closestCanoInfo;
	}
	
	public int getBestScore() {
		int best = 0;
		for (Genotipo genome: this.population.genomes) {
			if (genome.bird.gameScore > best) {
				best = genome.bird.gameScore;
			}
		}
		return best;
	}
	
	public boolean populationDead() {
		for (Genotipo genome: this.population.genomes) {
			if (!genome.bird.isDead) {
				return false;
			}
		}
		return true;
	}
	
	private void getClosestCano(List<Cano> pipes) {
		Cano closestCano = null;
		float distance = Float.MAX_VALUE;
		for (Cano pipe: pipes) {
			float test = pipe.x + pipe.width/2 - BirdFactory.getSpawnX();
			if (Math.abs(test) < Math.abs(distance)) {
				distance = test;
				closestCano = pipe;
			}
		}
		this.closestCanoInfo = new CanoInfo(distance, closestCano);
	}
}