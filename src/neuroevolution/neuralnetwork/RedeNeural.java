package neuroevolution.neuralnetwork;

import java.util.ArrayList;
import java.util.List;

public class RedeNeural {
	
	public class FlattenNetwork {
		public List<Integer> Neuronios;
		public List<Float> weights;
		
		public FlattenNetwork() {
			this.Neuronios = new ArrayList<Integer>();
			this.weights = new ArrayList<Float>();
		}
	}

	public List<Camada> Camadas;
	
	private RedeNeural() {
		this.Camadas = new ArrayList<Camada>();
	}
	
	public RedeNeural(int... topolgy) {
		this();
		int prevInputs = 0;
		for (int i = 0; i < topolgy.length; i++) {
			this.Camadas.add(new Camada(topolgy[i], prevInputs));
			prevInputs = topolgy[i];
		}
	}
	
	public FlattenNetwork flatten() {
		FlattenNetwork net = new FlattenNetwork();
		for (Camada Camada: this.Camadas) {
			net.Neuronios.add(Camada.Neuronios.size());
			for (Neuronio Neuronio: Camada.Neuronios) {
				for (float weight: Neuronio.weights) {
					net.weights.add(weight);
				}
			}
		}
		return net;
	}
	
	public static RedeNeural expand(FlattenNetwork net) {
		RedeNeural nn = new RedeNeural();
		int prevInput = 0;
		int weightIndex = 0;
		for (int NeuronioCount: net.Neuronios) {
			Camada Camada = new Camada(NeuronioCount, prevInput);
			for (int i = 0; i < Camada.Neuronios.size(); i++) {
				for (int j = 0; j < Camada.Neuronios.get(i).weights.size(); j++) {
					Camada.Neuronios.get(i).weights.set(j, net.weights.get(weightIndex++));
				}
			}
			prevInput = NeuronioCount;
			nn.Camadas.add(Camada);
		}
		return nn;
	}
	
	public float[] eval(float... inputs) {
		for (int i = 0; i < inputs.length; i++) {
			this.Camadas.get(0).Neuronios.get(i).value = inputs[i];
		}
		Camada prevCamada = this.Camadas.get(0);
		for (int i = 1; i < this.Camadas.size(); i++) {
			this.Camadas.get(i).eval(prevCamada);
			prevCamada = this.Camadas.get(i);
		}
		// prev Camada is now the last Camada in the network
		return prevCamada.getOutput();
	}
}