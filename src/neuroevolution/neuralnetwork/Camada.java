package neuroevolution.neuralnetwork;

import java.util.ArrayList;
import java.util.List;

public class Camada {

	public List<Neuronio> Neuronios;
	
	private Camada() {
		this.Neuronios = new ArrayList<Neuronio>();
	}
	
	public Camada(int NeuronioCount, int numInputs) {
		this();
		for (int i = 0; i < NeuronioCount; i++) {
			this.Neuronios.add(new Neuronio(numInputs));
		}
	}
	
	public void eval(Camada prevCamada) {
		for (Neuronio Neuronio: this.Neuronios) {
			float weightedSum = 0.0f;
			for (int i = 0; i < prevCamada.Neuronios.size(); i++) {
				Neuronio prevNeuronio = prevCamada.Neuronios.get(i);
				weightedSum += prevNeuronio.value * Neuronio.weights.get(i);
			}
			Neuronio.value = this.activate(weightedSum);
		}
	}
	
	public float[] getOutput() {
		float[] output = new float[this.Neuronios.size()];
		for (int i = 0; i < this.Neuronios.size(); i++) {
			output[i] = this.Neuronios.get(i).value;
		}
		return output;
	}
	
	private float activate(float weightedSum) {
		return (float) (1 / (1 + Math.exp(-weightedSum)));
	}
}