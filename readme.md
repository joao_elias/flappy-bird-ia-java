# Trabalho de Inteligência Artificial

## Descrição

Feito com a biblioteca gráfica Processing (https://processing.org/). O arquivo "core.jar" é a biblioteca gráfica. Para usar seus recursos no eclipse, expandir projeto, clicar com botão direito no "core.jar", Build Path, Add to Build Path. 

## Membros da equipe

- MAX YURI SILVA SOUSA - 201751159353
- JOAO ELIAS FERRAZ SANTANA - 201851084584
- FERNANDA DANIELLE DE OLIVEIRA - 201651230455
- THIAGO SILVA LAFITE LIMA - 201551319837
- ANTONIO CICERO PEREIRA DE JESUS - 201751129446
